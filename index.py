# homework 1

dict1 = {1: 10, 2: 20}
dict2 = {3: 30, 4: 40}
dict3 = {5: 50, 6: 60}
dict4 = {6: 60, 7: 70}

dict1.update(dict2)
dict1.update(dict3)
dict1.update(dict4)

print(dict1)

# homework 2

n = int(input("tell me number: "))
new_dict_1 = dict()
for i in range(1, n):
    new_dict_1[i] = i * i

print(new_dict_1)

# homework 3

new_dict_2 = {'c1': 'Red', 'c2': 'Green', 'c3': None}

for i in new_dict_2.copy():
    if new_dict_2[i] is None:
        new_dict_2.pop(i)

print(new_dict_2)

# homework 4

string = "Hi there, there is new level of python coming soon."
string = string.replace(',', '')
print(string)
counts = dict()
words = string.split()
for word in words:
    if word in counts:
        counts[word] += 1
    else:
        counts[word] = 1

print(counts)